package aka

import (
	"context"
	"crypto/sha256"
	"fmt"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	akaC "gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func dataSourceSubnet() *schema.Resource {
	return &schema.Resource{
		ReadContext: dataSourceSubnetRead,
		Schema: map[string]*schema.Schema{
			"cidr": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "CIDR",
			},
			"name": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Display name",
			},
			"gateway": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Gateway IP",
			},
			"sharednetwork": {
				Type:        schema.TypeBool,
				Computed:    true,
				Description: "Shared Network label",
			},
			"dhcpservers": {
				Type:        schema.TypeList,
				Computed:    true,
				Description: "DHCP Servers",
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"name": {
							Type:        schema.TypeString,
							Computed:    true,
							Description: "Hostname",
						},
						"ip": {
							Type:        schema.TypeString,
							Computed:    true,
							Description: "IP",
						},
					},
				},
			},
		},
	}
}

func dataSourceSubnetRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	c := m.(*akaC.Client)
	var diags diag.Diagnostics

	cidr := d.Get("cidr").(string)

	subnet, err := c.GetSubnet(ctx, cidr)
	if err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("name", subnet.Name); err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("gateway", subnet.Gateway); err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("sharednetwork", subnet.SharedNetwork); err != nil {
		return diag.FromErr(err)
	}

	dhcpservers := make([]map[string]interface{}, 0)
	for _, v := range subnet.DHCPServers {
		dhcpserver := make(map[string]interface{})
		dhcpserver["name"] = v.Name
		dhcpserver["ip"] = v.IP
		dhcpservers = append(dhcpservers, dhcpserver)
	}
	if err := d.Set("dhcpservers", dhcpservers); err != nil {
		return diag.FromErr(err)
	}
	d.SetId(fmt.Sprintf("%x", sha256.Sum256([]byte(cidr))))

	return diags
}

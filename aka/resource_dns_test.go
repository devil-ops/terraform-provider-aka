package aka

import (
	"fmt"
	"testing"

	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/resource"
	"github.com/hashicorp/terraform-plugin-sdk/v2/terraform"
)

func TestAccAKADnsCNAME(t *testing.T) {
	resource.Test(t, resource.TestCase{
		PreCheck:     func() { testAccPreCheck(t) },
		Providers:    testAccProviders,
		CheckDestroy: testAccCheckAKADnsDestroy,
		Steps: []resource.TestStep{
			{
				Config: testAccCheckAKADnsConfigCNAME("drews-aka-test-55.oit.duke.edu", "drews-dev-01.oit.duke.edu"),
				Check: resource.ComposeTestCheckFunc(
					testAccCheckAKADnsExists("aka_dns.new_cname"),
				),
			},
		},
	})
}

func TestAccAKADnsA(t *testing.T) {
	resource.Test(t, resource.TestCase{
		PreCheck:     func() { testAccPreCheck(t) },
		Providers:    testAccProviders,
		CheckDestroy: testAccCheckAKADnsDestroy,
		Steps: []resource.TestStep{
			{
				Config: testAccCheckAKADnsConfigA("drews-aka-test-55.oit.duke.edu", "10.138.3.167"),
				Check: resource.ComposeTestCheckFunc(
					testAccCheckAKADnsExists("aka_dns.new_a"),
				),
			},
		},
	})
}

func testAccCheckAKADnsDestroy(s *terraform.State) error {
	// c := testAccProvider.Meta().(*akaC.Client)

	for _, rs := range s.RootModule().Resources {
		if rs.Type != "aka_dns" {
			continue
		}
	}

	return nil
}

func testAccCheckAKADnsConfigCNAME(cname string, target string) string {
	return fmt.Sprintf(`
resource "aka_dns" "new_cname" {
  name = "%s"
  view = "Internal"
  target = "%s"
  type = "CNAME"
}`, cname, target)
}

func testAccCheckAKADnsConfigA(name string, address string) string {
	return fmt.Sprintf(`
resource "aka_dns" "new_a" {
  name = "%s"
  view = "Internal"
  address = "%s"
  type = "A"
}`, name, address)
}

func testAccCheckAKADnsExists(n string) resource.TestCheckFunc {
	return func(s *terraform.State) error {
		rs, ok := s.RootModule().Resources[n]

		if !ok {
			return fmt.Errorf("Not found: %s", n)
		}

		if rs.Primary.ID == "" {
			return fmt.Errorf("No ID set")
		}

		return nil
	}
}

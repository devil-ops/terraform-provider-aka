package aka

import (
	"context"
	"crypto/sha256"
	"fmt"
	"log/slog"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	akaC "gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func resourceDNS() *schema.Resource {
	return &schema.Resource{
		CreateContext: resourceDNSCreate,
		ReadContext:   resourceDNSRead,
		UpdateContext: resourceDNSUpdate,
		DeleteContext: resourceDNSDelete,
		Schema: map[string]*schema.Schema{
			"name": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "Display name for DNS record",
			},
			"view": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "DNS View = ['External', 'Internal']",
			},
			"type": {
				Type:        schema.TypeString,
				Required:    true,
				ForceNew:    true,
				Description: "Record type = ['A', 'AAAA', 'AFSDB', 'CAA', 'CERT', 'CNAME', 'DHCID', 'DLV', 'DNAME', 'DNSKEY', 'DS', 'HINFO', 'LOC', 'MX', 'NAPTR', 'NS', 'NSEC', 'PTR', 'RP', 'RRSIG', 'SRV', 'TKEY', 'TSIG', 'TXT']",
			},
			"address": {
				Type:        schema.TypeString,
				Optional:    true,
				Description: "IP this record points to (A and AAAA only)",
			},
			"target": {
				Type:        schema.TypeString,
				Optional:    true,
				Description: "Raw DNS name for where this record points (CNAME, MX, NS, PTR, and SRV only)",
			},
			"ttl": {
				Type:        schema.TypeInt,
				Default:     3600,
				Optional:    true,
				Description: "TTL (in seconds)",
			},
			"backendname": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Raw DNS record name",
			},
			"zonebackendname": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Raw DNS record name for zone",
			},
			"zonename": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Display name for DNS zone record exists in",
			},
		},
	}
}

func resourceDNSCreate(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	var diags diag.Diagnostics
	c := m.(*akaC.Client)

	if _, err := c.GetDNSRecordSpecific(ctx, d.Get("name").(string), d.Get("view").(string)); err == nil {
		diags = append(diags, diag.Diagnostic{
			Severity: diag.Error,
			Summary:  fmt.Sprintf("DNS entry already registered: %s", d.Get("name").(string)),
			Detail:   "This DNS entry is already registered, either delete, import, or choose another name",
		})
		return diags
	}

	recordType := d.Get("type").(string)
	if target := d.Get("target").(string); recordType == "CNAME" && target == "" {
		diags = append(diags, diag.Diagnostic{
			Severity: diag.Error,
			Summary:  "'target' is required with CNAME type",
			Detail:   "CNAME records need a target boss, make sure you set that, then try again",
		})
		return diags
	}
	if address := d.Get("address").(string); recordType == "A" && address == "" {
		diags = append(diags, diag.Diagnostic{
			Severity: diag.Error,
			Summary:  "'address' is required with A type",
			Detail:   "A records need an boss, make sure you set that, then try again",
		})
		return diags
	}
	// Get parameters ready
	params := &akaC.DNSParams{
		Name: d.Get("name").(string),
		View: d.Get("view").(string),
		TTL:  d.Get("ttl").(int),
		Type: d.Get("type").(string),
	}
	if d.Get("target").(string) != "" {
		params.Target = d.Get("target").(string)
	}
	if d.Get("address").(string) != "" {
		params.Address = d.Get("address").(string)
	}
	item, err := c.CreateDNSRecord(context.Background(), *params)
	if err != nil {
		// return diag.FromErr(err)
		diags = append(diags, diag.Diagnostic{
			Severity: diag.Error,
			Summary:  "Could not create DNS record",
			Detail:   fmt.Sprintf("Issues with request %+v", params),
		})
	}
	slog.Debug("got item", "item", item)
	resourceDNSRead(ctx, d, m)
	return diags
}

func resourceDNSRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	var diags diag.Diagnostics
	c := m.(*akaC.Client)
	targetName := d.Get("name").(string)
	targetView := d.Get("view").(string)

	slog.Info("Doing a DNS Read!!!!!!!")
	dns, err := c.GetDNSRecordSpecific(ctx, targetName, targetView)
	if err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("type", dns.Type); err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("address", dns.Address); err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("zonebackendname", dns.ZoneBackendName); err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("zonename", dns.ZoneName); err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("backendname", dns.BackendName); err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("ttl", dns.TTL); err != nil {
		return diag.FromErr(err)
	}
	sum := sha256.Sum256([]byte(fmt.Sprintf("%s-%s", targetName, targetView)))
	d.SetId(fmt.Sprintf("%x", sum))
	return diags
}

func resourceDNSUpdate(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	c := m.(*akaC.Client)
	targetName := d.Get("name").(string)
	targetView := d.Get("view").(string)
	dns, err := c.GetDNSRecordSpecific(ctx, targetName, targetView)

	params := &akaC.DNSParams{
		Name: d.Get("name").(string),
		View: d.Get("view").(string),
	}
	if err != nil {
		return diag.FromErr(err)
	}
	if d.HasChange("ttl") {
		params.TTL = d.Get("ttl").(int)
	} else {
		params.TTL = dns.TTL
	}
	if d.HasChange("target") {
		params.Target = d.Get("target").(string)
	}
	if d.HasChange("type") {
		params.Type = d.Get("type").(string)
	}
	if d.HasChange("address") {
		params.Address = d.Get("address").(string)
	}

	resp, err := c.UpdateDNSRecord(ctx, *params, dns.EditPath)
	if err != nil {
		return diag.FromErr(err)
	}
	slog.Info("got response", "response", resp)

	return resourceDNSRead(ctx, d, m)
}

func resourceDNSDelete(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	var diags diag.Diagnostics
	c := m.(*akaC.Client)
	targetName := d.Get("name").(string)
	targetView := d.Get("view").(string)
	dns, err := c.GetDNSRecordSpecific(ctx, targetName, targetView)
	if err != nil {
		return diag.FromErr(err)
	}
	deleteResp, err := c.DeleteDNSRecord(ctx, dns.EditPath)
	if err != nil {
		return diag.FromErr(err)
	}
	slog.Debug("delete response", "response", deleteResp)
	d.SetId("")

	return diags
}

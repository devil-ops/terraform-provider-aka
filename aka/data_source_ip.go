package aka

import (
	"context"
	"crypto/sha256"
	"fmt"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	akaC "gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func dataSourceIP() *schema.Resource {
	return &schema.Resource{
		ReadContext: dataSourceIPRead,
		Schema: map[string]*schema.Schema{
			"ip": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "IP Address",
			},
			"subnet": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Subnet - CIDR for v6, Prefix for v6",
			},
			"macaddress": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "MAC Address assigned to IP - possibly null",
			},
			"state": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "IP State = ['blocked', 'broadcast', 'dynamic_dhcp', 'free', 'gateway', 'network_address', 'router_anycast', 'static', 'static_dhcp']",
			},
		},
	}
}

func dataSourceIPRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	c := m.(*akaC.Client)
	var diags diag.Diagnostics

	// ip := d.Get("ip").(string)
	targetIP := d.Get("ip").(string)

	ip, err := c.ReadIP(ctx, targetIP)
	if err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("ip", ip.IP); err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("macaddress", ip.MacAddress); err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("subnet", ip.Subnet); err != nil {
		return diag.FromErr(err)
	}
	if err := d.Set("state", ip.State); err != nil {
		return diag.FromErr(err)
	}

	// Just some rando
	// d.SetId(strconv.FormatInt(time.Now().Unix(), 10))
	d.SetId(fmt.Sprintf("%x", sha256.Sum256([]byte(targetIP))))

	return diags
}

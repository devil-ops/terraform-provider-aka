package aka

import (
	"context"
	"crypto/sha256"
	"fmt"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	akaC "gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func dataSourceDNS() *schema.Resource {
	return &schema.Resource{
		ReadContext: dataSourceDNSRead,
		Schema: map[string]*schema.Schema{
			"name": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "Display name of DNS record",
			},
			"type": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Record type = ['A', 'AAAA', 'AFSDB', 'CAA', 'CERT', 'CNAME', 'DHCID', 'DLV', 'DNAME', 'DNSKEY', 'DS', 'HINFO', 'LOC', 'MX', 'NAPTR', 'NS', 'NSEC', 'PTR', 'RP', 'RRSIG', 'SRV', 'TKEY', 'TSIG', 'TXT']",
			},
			"ttl": {
				Type:        schema.TypeInt,
				Computed:    true,
				Description: "TTL (in seconds)",
			},
			"view": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "View for record (Typically Internal or External)",
			},
			"address": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "IP (A, AAAA only)",
			},
			"rdata": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "RData (HINFO, TXT, RP, AFSDB, LOC, NAPTR, CERT, DNAME, DS, RRSIG, NSEC, DNSKEY, DHCID, TKEY, TSIG, CAA, DLV only)",
			},
			"backendname": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Raw DNS record name",
			},
			"zonebackendname": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Raw DNS record name for zone",
			},
			"zonename": {
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Display name for DNS zone record exists in",
			},
		},
	}
}

func dataSourceDNSRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	c := m.(*akaC.Client)
	var diags diag.Diagnostics

	// ip := d.Get("ip").(string)
	targetName := d.Get("name").(string)
	targetView := d.Get("view").(string)

	dnses, err := c.GetDNSRecord(ctx, targetName)
	if err != nil {
		return diag.FromErr(err)
	}
	for _, dns := range dnses {
		if dns.View != targetView {
			continue
		}
		if err := d.Set("type", dns.Type); err != nil {
			return diag.FromErr(err)
		}
		if err := d.Set("address", dns.Address); err != nil {
			return diag.FromErr(err)
		}
		if err := d.Set("zonebackendname", dns.ZoneBackendName); err != nil {
			return diag.FromErr(err)
		}
		if err := d.Set("rdata", dns.Rdata); err != nil {
			return diag.FromErr(err)
		}
		if err := d.Set("zonename", dns.ZoneName); err != nil {
			return diag.FromErr(err)
		}
		if err := d.Set("backendname", dns.BackendName); err != nil {
			return diag.FromErr(err)
		}
		if err := d.Set("ttl", dns.TTL); err != nil {
			return diag.FromErr(err)
		}
		d.SetId(fmt.Sprintf("%x", sha256.Sum256([]byte(fmt.Sprintf("%s-%s", targetName, targetView)))))
		diags = append(diags, diag.Diagnostic{
			Severity: diag.Error,
			Summary:  "Unable to find aka_dns",
			Detail:   "Could not find the aka_dns entry, are you sure it exists?",
		})
	}

	return diags
}

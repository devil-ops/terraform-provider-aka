/*
Package aka is the provider for an AKA thing
*/
package aka

import (
	"fmt"
	"strings"

	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"

	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
)

func init() {
	schema.DescriptionKind = schema.StringMarkdown

	schema.SchemaDescriptionBuilder = func(s *schema.Schema) string {
		desc := s.Description
		if s.Default != nil {
			desc += fmt.Sprintf(" Defaults to `%v`.", s.Default)
		}
		if s.Deprecated != "" {
			desc += " " + s.Deprecated
		}
		return strings.TrimSpace(desc)
	}
}

// Provider is the main thing this module 'provides'
func Provider() *schema.Provider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"url": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("AKA_URL", "https://aka.oit.duke.edu"),
				Description: "URL for AKA Server.  Defaults to https://aka.oit.duke.edu",
			},
			"user": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("AKA_USER", ""),
				Description: "Username for AKA calls",
			},
			"key": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("AKA_KEY", ""),
				Description: "API Key for authenticating against AKA",
			},
		},
		ResourcesMap: map[string]*schema.Resource{
			"aka_dns": resourceDNS(),
		},
		DataSourcesMap: map[string]*schema.Resource{
			"aka_subnet": dataSourceSubnet(),
			"aka_ip":     dataSourceIP(),
			"aka_dns":    dataSourceDNS(),
		},
		ConfigureFunc: providerConfigure,
	}
}

func providerConfigure(d *schema.ResourceData) (interface{}, error) {
	return aka.NewClient(&aka.AKAConfig{
		BaseURL: d.Get("url").(string),
		APIUser: d.Get("user").(string),
		APIKey:  d.Get("key").(string),
	}), nil

	// return client.NewClient(address, port, token), nil
}

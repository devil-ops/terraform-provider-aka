resource "aka_dns" "example-cname" {
  name = "cname-example.oit.duke.edu"
  view = "Internal"
  target = "cname-example-01.oit.duke.edu"
  type = "CNAME"
}

resource "aka_dns" "example-arecord" {
  name = "arecord-example.oit.duke.edu"
  view = "Internal"
  type = "A"
  address = "1.2.3.4"
}

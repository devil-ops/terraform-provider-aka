provider "aka" {
  url = var.url # Defaults to https://aka.oit.duke.edu
  user = var.user # optionally set with AKA_USER
  key = var.key # optionally set with AKA_KEY
}

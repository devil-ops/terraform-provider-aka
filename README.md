# terraform-provider-aka

Manipulate AKA DNS records using Terraform

## How do I use this?

You can download and run `make` to compile locally.  This is great for testing, check out the [sandbox/main.tf](sandbox/main.tf) file for an example of testing stuff out.

You can also just specify it like this in your project (Probably needs to be installed locally...?)

```
terraform {
  required_providers {
    aka = {
      version = "0.0.1"
      source = "gitlab.oit.duke.edu/devil-ops/aka"
    }
  }
}

provider "aka" {
}
```

Check out full documentation in the [Docs](/docs) directory

## Generate Documentation

Docs are located in docs/.  These are autogenerated using a tool called `tfplugindocs`.

Install the tfplugindocs tool using:

```
$ go get github.com/hashicorp/terraform-plugin-docs/cmd/tfplugindocs
```

Generate the documentation at the root of this repository

```
$ tfplugindocs generate
```

### Disclaimer

*This code is freely available for non-commercial use and is provided as-is with no warranty.*

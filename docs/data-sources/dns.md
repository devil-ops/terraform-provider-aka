---
# generated by https://github.com/hashicorp/terraform-plugin-docs
page_title: "aka_dns Data Source - terraform-provider-aka"
subcategory: ""
description: |-
  
---

# aka_dns (Data Source)



## Example Usage

```terraform
data "aka_dns" "example" {
  name = "example-01.oit.duke.edu"
  view = "Internal"
}
```

<!-- schema generated by tfplugindocs -->
## Schema

### Required

- **name** (String) Display name of DNS record
- **view** (String) View for record (Typically Internal or External)

### Optional

- **id** (String) The ID of this resource.

### Read-Only

- **address** (String) IP (A, AAAA only)
- **backendname** (String) Raw DNS record name
- **rdata** (String) RData (HINFO, TXT, RP, AFSDB, LOC, NAPTR, CERT, DNAME, DS, RRSIG, NSEC, DNSKEY, DHCID, TKEY, TSIG, CAA, DLV only)
- **ttl** (Number) TTL (in seconds)
- **type** (String) Record type = ['A', 'AAAA', 'AFSDB', 'CAA', 'CERT', 'CNAME', 'DHCID', 'DLV', 'DNAME', 'DNSKEY', 'DS', 'HINFO', 'LOC', 'MX', 'NAPTR', 'NS', 'NSEC', 'PTR', 'RP', 'RRSIG', 'SRV', 'TKEY', 'TSIG', 'TXT']
- **zonebackendname** (String) Raw DNS record name for zone
- **zonename** (String) Display name for DNS zone record exists in



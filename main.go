/*
Package main is the main executable
*/
package main

import (
	"github.com/hashicorp/terraform-plugin-sdk/v2/plugin"
	"gitlab.oit.duke.edu/devil-ops/terraform-provider-aka/aka"
)

func main() {
	plugin.Serve(&plugin.ServeOpts{
		ProviderFunc: aka.Provider,
	})
}

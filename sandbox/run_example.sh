#!/bin/bash
set -x
set -e

make -C ../
rm -rf .terraform .terraform.lock.hcl terraform.tfstate terraform.tfstate.backup
terraform init && terraform apply

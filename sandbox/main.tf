terraform {
  required_providers {
    aka = {
      version = "0.0.1"
      source = "gitlab.oit.duke.edu/devil-ops/aka"
    }
  }
}

provider "aka" {
}

data "aka_subnet" "test" {
cidr = "10.200.192.64/26"
}
//
data "aka_ip" "test" {
  ip = "10.138.3.167"
}
//
//output "aka_subnet" {
  //value = data.aka_subnet.test
//}
//
//output "aka_ip" {
  //value = data.aka_ip.test
//}
//
data "aka_dns" "test" {
  name = "drews-dev-01.oit.duke.edu"
  view = "Internal"
}

resource "aka_dns" "test" {
  name = "drews-dev-terraform-aka-01.oit.duke.edu"
  view = "Internal"
  target = "drews-dev-01.oit.duke.edu"
  type = "CNAME"
}

//output "aka_dns_resource" {
//value = aka_dns.test
//}
